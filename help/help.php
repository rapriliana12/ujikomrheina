<!DOCTYPE html>
<html>
<head>
	<title>Help!</title>
</head>
<body>
 <div class="col-md-4 col-sm-4">          
            <div class="panel panel-back noti-box">
                <div class="text-box" >
                	<h3>MANUAL PROGRAM</h3>
                   <table width="42%" border="1">
                   	<tr>
                   		<td>1.</td>
                   		<td>Kita harus login sesuai dengan username dan password yang sudah ditentukan. Login sebagai Admin, username : admin, password : admin1. Login sebagai Operator, username : Operator, password : admin2</td>
                   	</tr>
                   	<tr>
                   		<td>2.</td>
                   		<td>Saat login sebagai Admin, anda akan langsung masuk ke halaman index. Dimana pada halaman ini terdapat form tambah data inventaris, dan data yang kita inputkan pada form tersebut akan masuk ke dalam tabel inventaris yang berada dibawah form.</td>
                   	</tr>
                   	<tr>
                   		<td>3.</td>
                   		<td>Selain itu, data yang di inputkan tersebut pun akan masuk ke dalam tabel sesuai jurusan yang berada pada fitur data barang.</td>
                   	</tr>
                   	<tr>
                   		<td>4.</td>
                   		<td>Pada fitur peminjaman, anda bisa meminjam data dengan menginputkan data pada form peminjaman dan data pun akan masuk ke dalam tabel data peminjaman. Dan jumlah barang yang dipinjam otomatis akan berkurang.</td>
                   	</tr>
                   	<tr>
                   		<td>5.</td>
                   		<td>Pada fitur pengembalian, data hanya bisa dilihat apabila anda sudah meng klik tombol kembalikan yang ada pada data peminjaman.</td>
                   	</tr>
                   	<tr>
                   		<td>6.</td>
                   		<td>Pada fitur laporan, terdapat tiga data yang akan menjadi sebuah laporan(data barang, data peminjam dan data pengembalian) disini anda bisa mengexport nya ke excel atau pdf.</td>
                   	</tr>
                   	<tr>
                   		<td>7.</td>
                   		<td>Pada fitur backup database, kita bisa mendownload semua database yang digunakan pada website ini.</td>
                   	</tr>
                   	<tr>
                   		<td>8.</td>
                   		<td>Keterangan : Masih banyak fitur yang belum berjalan dengan baik. Seperti sebagian data peminjaman ada yang belum tampil, jumlah data saat dikembalikan belum bertambah dan fitur backup database belum berfungsi</td>
                   	</tr>
                   </table>
            
                </div>
             </div>
        </div>
</body>
</html>