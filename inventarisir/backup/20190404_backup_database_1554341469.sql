DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(15) NOT NULL,
  `jumlah` int(50) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` varchar(50) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` varchar(50) NOT NULL,
  `kode_inventaris` varchar(50) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `id_petugas` varchar(50) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("32","PC","Baik","ANM","40","34","2019-04-04 08:23:43","6","KD 001","0","1");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("33","Bola Besar","KJ 002","Alat Olahraga");
INSERT INTO jenis VALUES("34","PC","KJ 003","Elektronik");
INSERT INTO jenis VALUES("35","Laptop Acer","KJ 004","Elektronik");
INSERT INTO jenis VALUES("36","Laptop Lenovo","KJ 005","Elektronik");



DROP TABLE jurusan;

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jurusan` varchar(50) NOT NULL,
  `ket_jurusan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jurusan`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO jurusan VALUES("1","RPL","Rekayasa Perangkat Lunak");
INSERT INTO jurusan VALUES("2","ANM","Animasi");
INSERT INTO jurusan VALUES("3","TKR","Teknik Kendaraan Ringan");
INSERT INTO jurusan VALUES("4","BC","Broadcasting");
INSERT INTO jurusan VALUES("5","TPL","Teknik Pengelasan");
INSERT INTO jurusan VALUES("6","Lainnya","Lainnya");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","Admin");
INSERT INTO level VALUES("2","Petugas");
INSERT INTO level VALUES("3","Pegawai");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` char(15) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Rizki Muhtiani Putri","001987","Bogor");
INSERT INTO pegawai VALUES("2","Nabila Silfani","001879","Jakarta");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(15) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("19","30","Laptop-Acer23","2019-02-20 02:57:06","2019-02-25 09:12:02","Dikembalikan","1");
INSERT INTO peminjaman VALUES("20","30","Laptop-Acer23","2019-02-25 09:12:17","2019-04-01 03:39:17","Dikembalikan","1");
INSERT INTO peminjaman VALUES("25","30","Laptop-Acer23","2019-04-01 00:00:00","2019-04-01 06:06:20","Dikembalikan","1");
INSERT INTO peminjaman VALUES("26","28","Las","2019-04-01 04:13:45","2019-04-01 04:14:51","Dikembalikan","0");
INSERT INTO peminjaman VALUES("27","0","Headset","2019-04-01 05:58:56","2019-04-01 06:04:25","Dikembalikan","1");
INSERT INTO peminjaman VALUES("29","0","Headset","2019-04-01 06:00:20","2019-04-01 06:06:53","Dikembalikan","1");
INSERT INTO peminjaman VALUES("30","0","PC","2019-04-01 06:46:50","2019-04-01 06:48:32","Dikembalikan","2");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","admin1","Admin","1");
INSERT INTO petugas VALUES("2","operator","admin2","Operator","2");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("5","Lab RPL","KR 001","RPL");
INSERT INTO ruang VALUES("6","Lab ANM","KR 002","ANM");
INSERT INTO ruang VALUES("7","Bengkel TKR","KR 003","TKR");
INSERT INTO ruang VALUES("8","Studio BC","KR 004","BC");
INSERT INTO ruang VALUES("9","Bengkel LAS","KR 005","TPL");



