DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("3","32","1","38");
INSERT INTO detail_pinjam VALUES("4","32","3","39");
INSERT INTO detail_pinjam VALUES("5","32","3","41");
INSERT INTO detail_pinjam VALUES("6","32","3","41");
INSERT INTO detail_pinjam VALUES("7","32","1","42");
INSERT INTO detail_pinjam VALUES("8","32","2","43");
INSERT INTO detail_pinjam VALUES("9","32","1","44");
INSERT INTO detail_pinjam VALUES("10","32","1","45");
INSERT INTO detail_pinjam VALUES("11","34","1","46");
INSERT INTO detail_pinjam VALUES("12","32","1","47");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` varchar(50) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` varchar(50) NOT NULL,
  `kode_inventaris` varchar(50) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `id_petugas` varchar(50) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("32","PC","Baik","ANM","29","34","2019-04-04 08:23:43","6","KD 001","2","1");
INSERT INTO inventaris VALUES("34","Laptop Lenovo","Baik","Elektronik","40","33","2019-04-05 10:36:58","5","KD 002","1","1");
INSERT INTO inventaris VALUES("35","Laptop acer","Baik","RPL","40","36","2019-04-07 16:45:28","5","KD 003","1","1");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("33","Bola Besar","KJ 002","Alat Olahraga");
INSERT INTO jenis VALUES("34","PC","KJ 003","Elektronik");
INSERT INTO jenis VALUES("36","Laptop","KJ 005","Elektronik");



DROP TABLE jurusan;

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jurusan` varchar(50) NOT NULL,
  `ket_jurusan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jurusan`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO jurusan VALUES("1","RPL","Rekayasa Perangkat Lunak");
INSERT INTO jurusan VALUES("2","ANM","Animasi");
INSERT INTO jurusan VALUES("3","TKR","Teknik Kendaraan Ringan");
INSERT INTO jurusan VALUES("4","BC","Broadcasting");
INSERT INTO jurusan VALUES("5","TPL","Teknik Pengelasan");
INSERT INTO jurusan VALUES("6","Lainnya","Lainnya");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","Admin");
INSERT INTO level VALUES("2","Petugas");
INSERT INTO level VALUES("3","Pegawai");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` char(15) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Rizki Muhtiani Putri","001987","Bogor");
INSERT INTO pegawai VALUES("2","Nabila Silfani","001879","Jakarta");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("46","34","","2019-04-05 07:05:36","2019-04-05 07:27:38","Sudah Dikembalikan","2");
INSERT INTO peminjaman VALUES("47","32","","2019-04-06 09:31:11","2019-04-06 09:32:54","Sudah Dikembalikan","1");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","admin1","Admin","1");
INSERT INTO petugas VALUES("2","operator","admin2","Operator","2");
INSERT INTO petugas VALUES("3","user","admin3","Pegawai","3");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("5","Lab RPL","KR 001","RPL");
INSERT INTO ruang VALUES("6","Lab ANM","KR 002","ANM");
INSERT INTO ruang VALUES("7","Bengkel TKR","KR 003","TKR");
INSERT INTO ruang VALUES("8","Studio BC","KR 004","BC");
INSERT INTO ruang VALUES("9","Bengkel LAS","KR 005","TPL");



