<?php
include "../koneksi.php";
session_start();
if(isset($_SESSION['username'])){
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>INVENTARIS</title>
    <link href="../inventarisir/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../inventarisir/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../inventarisir/assets/css/custom.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../inventarisir/assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="../inventarisir/assets/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="../inventarisir/assets/datatables/dataTables.bootstrap4.js" rel="stylesheet"/>
    <link href="../inventarisir/assets/datatables/jquery.dataTables.js" rel="stylesheet"/>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="barang.php"><font size="5px">INVENSS</font></a> 
            </div>
  
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="assets/img/adm.png" class="user-image img-responsive"/>
                    <h2></h2>
                    </li>

                    
                     <li>
                        <a class=""  href=""><i class="fa fa-home fa-fw nav_icon"></i>Inventarisir</a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../inventarisir/data_barang.php">Data Barang</a>
                            </li>
                            <li>
                                <a href="../inventarisir/jenis.php">Jenis</a>
                            </li>
                            <li>
                                <a href="../inventarisir/ruang.php">Ruang</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="" href="../peminjaman/peminjam.php"><i class="fa fa-cloud-download fa-fw nav_icon"></i>Peminjaman</a>
                    </li>
                    <li>
                        <a class="" href="../pengembalian/kembali.php"><i class="fa fa-cloud-upload fa-fw nav_icon"></i>Pengembalian</a>
                    </li>
                     <li>
                        <a class=""  href=""><i class="fa fa-file fa-fw nav_icon"></i>Laporan</a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../laporan/laporan_dt_barang.php">Data Barang</a>
                            </li>
                            <li>
                                <a href="../laporan/laporan_peminjaman.php">Peminjaman</a>
                            </li>
                            <li>
                                <a href="../laporan/laporan_pengembalian.php">Pengembalian</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a class="" href="../inventarisir/backup_db2.php"><i class="fa fa-folder-open-o fa-fw nav_icon"></i>Backup Database</a>
                    </li>
                    <li>
                        <a class="" href="../logout.php"><i class=""></i>Logout</a>
                    </li>
                </ul>
               
            </div>
            
        </nav> 
<div id="page-wrapper">
    <div class="graphs">
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><b><center>TAMBAH DATA</center></b></div>
        <div class="panel-body">

        <div class="col-lg-6">
            <form action="proses_t.php" method="POST" enctype="multipart/form-data" >
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" name="nama" placeholder="" required="" / autocomplete="off">
                </div>

                <div class="form-group">
                    <label>Kondisi</label>
                    <select name="kondisi" class="form-control" required="" />  
                        <option value="Baik">Baik</option>
                        <option value="Rusak">Rusak</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" class="form-control" name="keterangan" required="" / autocomplete="off">
                </div>

                <div class="form-group">
                <label>Jumlah</label>
                    <input type="text"  name = "jumlah" class="form-control" required="" / autocomplete="off">
                </div>
                
                <div class="form-group">
                <label>Jurusan</label>
                     <select name="id_jurusan" class="form-control" required="" />
                        <?php 
                            include"../koneksi.php";
                            $sql=mysqli_query($konek,"SELECT * FROM jurusan");
                            while($data=mysqli_fetch_array($sql)){
                                if ($data['id_jurusan']==$tampil['id_jurusan']) {
                                    $selected="selected";
                                }else{
                                    $selected="";
                                }

                        ?>
                        <option value="<?php echo $data['id_jurusan'];?>" <?php echo $selected ?>><?php echo $data['nama_jurusan'];?></option>
                        <?php 
                        } ?>
                        
                    </select>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label>Jenis</label>
                      <select name="id_jenis" class="form-control" required="" />
                        <?php 
                            include"../koneksi.php";
                            $sql=mysqli_query($konek,"SELECT * FROM jenis");
                            while($data=mysqli_fetch_array($sql)){
                                if ($data['id_jenis']==$tampil['id_jenis']) {
                                    $selected="selected";
                                }else{
                                    $selected="";
                                }

                        ?>
                        <option value="<?php echo $data['id_jenis'];?>" <?php echo $selected ?>><?php echo $data['nama_jenis'];?></option>
                        <?php 
                        } ?>
                        
                    </select>
                </div>
                <div class="form-group">
                    <label>Ruang</label>
                    <select name="id_ruang" class="form-control" required="" />
                        <?php 
                            include"../koneksi.php";
                            $sql=mysqli_query($konek,"SELECT * FROM ruang");
                            while($data=mysqli_fetch_array($sql)){
                                if ($data['id_ruang']==$tampil['id_ruang']) {
                                    $selected="selected";
                                }else{
                                    $selected="";
                                }

                        ?>
                        <option value="<?php echo $data['id_ruang'];?>" <?php echo $selected ?>><?php echo $data['nama_ruang'];?></option>
                        <?php 
                        } ?>
                        
                    </select>
                </div>
                <div class="form-group">
                    <label>Kode Inventaris</label>
                    <input type="text" class="form-control" name="kode_inventaris" required="" / autocomplete="off">
                </div>
                <div class="form-group">
                    <label>Petugas</label>
                      <select name="id_petugas" class="form-control" required="" />
                        <?php 
                            include"../koneksi.php";
                            $sql=mysqli_query($konek,"SELECT * FROM petugas");
                            while($data=mysqli_fetch_array($sql)){
                                if ($data['id_petugas']==$tampil['id_petugas']) {
                                    $selected="selected";
                                }else{
                                    $selected="";
                                }

                        ?>
                        <option value="<?php echo $data['id_petugas'];?>" <?php echo $selected ?>><?php echo $data['nama_petugas'];?></option>
                        <?php 
                        } ?>
                        
                    </select>
                </div>
                <div class="form-group">
                        <input type="submit" name="simpan" value="Submit" class="btn btn-primary" >
                </div>
            </form>
        </div>
        </div>
        </div>  
        </div>
        </div>
     <div class="panel panel-default">
        <div class="panel-heading"><b><center>DATA INVENTARIS</center></b></div>
        <div class="panel-body">
        <br>
        <div class="table-responsive">
            <table id="dataTables-example" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Kondisi</td>
                        <td>Keterangan</td>
                        <td>Jumlah</td>
                        <td>Jenis</td>
                        <td>Tgl Register</td>
                        <td>Ruang</td>
                        <td>Kode Inventaris</td>
                        <td>Petugas</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    $pilih=mysqli_query($konek, "SELECT * FROM inventaris INNER JOIN jenis ON jenis.id_jenis=inventaris.id_jenis LEFT JOIN petugas ON inventaris.id_petugas=petugas.id_petugas LEFT JOIN ruang ON inventaris.id_ruang=ruang.id_ruang order by inventaris.id_inventaris  asc" );
                    while($data=mysqli_fetch_array($pilih)){
                    ?>
                    <tr>
                        <td><?=$no++; ?></td>
                        <td><?=$data['nama'];?></td>
                        <td><?=$data['kondisi'];?></td>
                        <td><?=$data['keterangan'];?></td>
                        <td><?=$data['jumlah'];?></td>
                        <td><?=$data['nama_jenis'];?></td>
                        <td><?=$data['tanggal_register'];?></td>
                        <td><?=$data['nama_ruang'];?></td>
                        <td><?=$data['kode_inventaris'];?></td>
                        <td><?=$data['nama_petugas'];?></td>
                        <td>
                            <a class="btn btn-success" href="ubah_in.php?id_inventaris=<?php echo $data['id_inventaris'];?>"><i class="fa fa-edit fa-fw nav_icon"></i></a>
                            <a onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini??')" class="btn btn-danger" href="hapus_in.php?id_inventaris=<?php echo $data['id_inventaris'];?>"><i class="fa fa-trash-o fa-fw nav_icon"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
           
        </div>
    </div>
</div>
</div>
</div>

        <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
<?php
}else{
    header("location:../login/login.php");
  }
  ?>