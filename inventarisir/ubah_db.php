<?php
include "../koneksi.php";
$pilih=mysqli_query($konek, " SELECT * FROM inventaris where id_inventaris='$_GET[id_inventaris]'");
$tampil =mysqli_fetch_array($pilih);
?>

<!DOCTYPE html>
<html>
<?php
include '../header.php';
?>
<head>
    <title></title>
</head>
<body>
<div id="page-wrapper">
    <div class="graphs">
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><b><center>UBAH DATA</center></b></div>
        <div class="panel-body">
        <div class="col-lg-6">
            <form action="proses_edit_db.php" method="post" enctype="multipart/form-data" >
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="hidden" name="id_inventaris" value="<?php echo $_GET['id_inventaris']?>">
                    <input type="text" class="form-control" name="nama" placeholder="" value="<?=$tampil['nama'];?>">
                </div>
                <div class="form-group">
                    <label>Kondisi</label>
                    <select name="kondisi" class="form-control">
                    <?php if ($tampil['kondisi']=='Baik') {
                        echo"<option value='Baik' selected>Baik</option>
                        <option value='Rusak'>Rusak</option>
                        <option value='Perbaikan'>Perbaikan</option>";
                    }elseif ($tampil['kondisi']=='Rusak') {
                        echo"<option value='Baik'>Baik</option>
                        <option value='Rusak' selected>Rusak</option>
                        <option value='Perbaikan'>Perbaikan</option>";
                    }elseif ($tampil['kondisi']=='Perbaikan') {
                        echo"<option value='Baik'>Baik</option>
                        <option value='Rusak'>Rusak</option>
                        <option value='Perbaikan' selected>Perbaikan</option>";
                    }else{
                        echo"<option value='Baik' selected>Baik</option>
                        <option value='Rusak' selected>Rusak</option>";
                    } ?> 
                    </select>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" class="form-control" name="keterangan" value="<?=$tampil['keterangan'];?>">
                </div>
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" class="form-control" name="jumlah" placeholder="" value="<?=$tampil['jumlah'];?>">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Jenis</label>
                    <select name="id_jenis" class="form-control">
                        <?php 
                            include"../koneksi.php";
                            $sql=mysqli_query($konek,"SELECT * FROM jenis");
                            while($data=mysqli_fetch_array($sql)){
                                if ($data['id_jenis']==$tampil['id_jenis']) {
                                    $selected="selected";
                                }else{
                                    $selected="";
                                }

                        ?>
                        <option value="<?php echo $data['id_jenis'];?>" <?php echo $selected ?>><?php echo $data['nama_jenis'];?></option>
                        <?php 
                        } ?>
                        
                    </select>
                </div>
                <div class="form-group">
                    <label>Ruang</label>
                    <input type="text" class="form-control" name="id_ruang" value="<?=$tampil['id_ruang'];?>">
                </div>
                <div class="form-group">
                    <label>Kode Inventaris</label>
                    <input type="text" class="form-control" name="kode_inventaris" value="<?=$tampil['kode_inventaris'];?>">
                </div>
                <div class="form-group">
                    <label>Petugas</label>
                    <input type="text" class="form-control" name="id_petugas" value="<?=$tampil['id_petugas'];?>">
                </div>
                <div class="form-group">
                        <input type="submit" name="edit" value="edit" class="btn btn-primary" >
                        <a href="index.php" type="submit" name="batal" value="batal" class="btn btn-success" >Batal</a>
                </div>
            </form>
        </div>
        </div>
        </div>
        </div>
        </div>

</body>
</html>