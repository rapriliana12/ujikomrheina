<?php
include '../koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);


$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Barang",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Kondisi', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Keterangan', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Jumlah', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Jenis', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tgl Register', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Ruang', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Kode Inventaris', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Petugas', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($konek, "SELECT inventaris.*,ruang.id_ruang,jenis.id_jenis,petugas.id_petugas,inventaris.keterangan FROM `inventaris` JOIN ruang ON inventaris.id_ruang=ruang.id_ruang JOIN jenis ON jenis.id_jenis=inventaris.id_jenis JOIN petugas ON petugas.id_petugas=inventaris.id_petugas ORDER BY inventaris.id_inventaris DESC");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['kondisi'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['keterangan'], 1, 0,'C');
	$pdf->Cell(2, 0.8, $lihat['jumlah'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['id_jenis'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_register'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['id_ruang'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['kode_inventaris'],1, 0, 'C');
	$pdf->Cell(3., 0.8, $lihat['id_petugas'],1, 1, 'C');

	$no++;
}

$pdf->Output("laporan_inventaris.pdf","I");

?>


