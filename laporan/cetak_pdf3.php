<?php
include '../koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(6,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);


$pdf->SetFont('Arial','B',14);
$pdf->Cell(16.5,0.7,"Laporan Data Pengembalian",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal Kembali', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Status Peminjaman', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Peminjam', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$pilih=mysqli_query($konek, "SELECT * FROM peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai=pegawai.id_pegawai JOIN detail_pinjam ON detail_pinjam.id_peminjaman=peminjaman.id_peminjaman left join inventaris on peminjaman.id_inventaris=inventaris.id_inventaris where status_peminjaman ='Sudah Dikembalikan'  order by tanggal_kembali desc");
    while($lihat=mysqli_fetch_array($pilih)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_kembali'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['status_peminjaman'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['nama_pegawai'],1, 1, 'C');

	$no++;
}

$pdf->Output("laporan_peminjaman.pdf","I");

?>


