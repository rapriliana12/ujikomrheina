<?php
include "../koneksi.php";
?>
<!DOCTYPE HTML>
<html lang="zxx">

<head>
	<title>INVENTARIS</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Portrait Login Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements"
	/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="css/font-awesome.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	<!-- online-fonts -->
	<link href="//fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<!-- //online-fonts -->
</head>

<body>
	<!--header-->
	<div class="header-w3l">
		<h1>
			<span>L</span>ogin
	</div>
	<!--//header-->
	<div class="main-content-agile">
		<div class="sub-main-w3">
			<img src="../inventarisir/assets/img/admin1.png" height="30%" width="30%" alt="" />
			<h2></h2>
			<form action="cek_login.php" method="post">
				<div class="pom-agile">
					<span class="fa fa-user-o" aria-hidden="true"></span>
					<input id="name" name="username" placeholder="Username" type="text" required="" autocomplete="off">
				</div>
				<div class="pom-agile">
					<span class="fa fa-key" aria-hidden="true"></span>
					<input placeholder="Password" name="password" type="password" required="" autocomplete="off">
				</div>
				<div class="right-w3l">
					<input type="submit" name="submit" id="submit" value="Login">
				</div>
				<div class="forgot-w3l">
					<a href="../help/help.php">Help?</a>
				</div>
			</form>
		</div>
	</div>
	<!--//main-->
	<!--footer-->
	<div class="footer">
    <p>Copyright &copy; 2018 SMKN 1 CIOMAS. All Rights Reserved | Design by W3layouts </p>
</div>
	</div>
	<!--//footer-->
</body>

</html>