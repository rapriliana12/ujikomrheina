<?php
include "../koneksi.php";
include "../header.php";
?>
<div id="page-wrapper">
    <div class="graphs">
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><b><center>Form Peminjam</center></b></div>
        <div class="panel-body">
        <div class="col-lg-6">
            <form action="proses_t.php" method="POST" enctype="multipart/form-data" >
                <div class="form-group">
                    <label>Peminjam</label>
                    <input type="text" class="form-control" name="nama" placeholder="">
                </div>
                <div class="form-group">
                    <label>Kondisi</label>
                    <select name="kondisi" class="form-control">  
                        <option value="Baik">Baik</option>
                        <option value="Rusak">Rusak</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" class="form-control" name="keterangan">
                </div>
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" class="form-control" name="jumlah" placeholder="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Jenis</label>
                    <select name="id_jenis" class="form-control">  
                        <option value="Elektronik">Elektronik</option>
                        <option value="Non Elektronik">Non Elektronik</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Ruang</label>
                    <input type="text" class="form-control" name="id_ruang">
                </div>
                <div class="form-group">
                    <label>Kode Inventaris</label>
                    <input type="text" class="form-control" name="kode_inventaris">
                </div>
                <div class="form-group">
                    <label>Petugas</label>
                    <input type="text" class="form-control" name="id_petugas">
                </div>
                <div class="form-group">
                        <input type="submit" name="simpan" value="Submit" class="btn btn-primary" >
                </div>
            </form>
        </div>
        </div>
        </div>
        </div>
        </div>
     <div class="panel panel-default">
        <div class="panel-heading"><b><center>DATA INVENTARIS</center></b></div>
        <div class="panel-body">
        <br>
        <div class="table-responsive">
            <table id="dataTables-example" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Kondisi</td>
                        <td>Keterangan</td>
                        <td>Jumlah</td>
                        <td>Jenis</td>
                        <td>Tgl Register</td>
                        <td>Ruang</td>
                        <td>Kode Inventaris</td>
                        <td>Petugas</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    $pilih=mysqli_query($konek, "SELECT * FROM inventaris");
                    while($data=mysqli_fetch_array($pilih)){
                    ?>
                    <tr>
                        <td><?=$no++; ?></td>
                        <td><?=$data['nama'];?></td>
                        <td><?=$data['kondisi'];?></td>
                        <td><?=$data['keterangan'];?></td>
                        <td><?=$data['jumlah'];?></td>
                        <td><?=$data['id_jenis'];?></td>
                        <td><?=$data['tanggal_register'];?></td>
                        <td><?=$data['id_ruang'];?></td>
                        <td><?=$data['kode_inventaris'];?></td>
                        <td><?=$data['id_petugas'];?></td>
                        <td>
                            <a class="btn btn-primary" href="ubah_in.php?id_inventaris=<?php echo $data['id_inventaris'];?>"><i class="fa fa-edit fa-fw nav_icon"></i></a>
                            <a onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini??')" class="btn btn-danger" href="hapus_in.php?id_inventaris=<?php echo $data['id_inventaris'];?>"><i class="fa fa-trash-o fa-fw nav_icon"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
           
        </div>
    </div>
</div>
</div>
</div>
